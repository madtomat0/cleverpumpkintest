//
//  CPTTripData.h
//  CleverPumpkinTest
//
//  Created by MAD Tomato on 11.07.13.
//  Copyright (c) 2013 MAD Tomato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CPTTripData : NSObject

@property (nonatomic, strong) NSString* takeoffCity;
@property (nonatomic, strong) NSString* landingCity;
@property (nonatomic, strong) NSString* takeoffDate;
@property (nonatomic, strong) NSString* landingDate;
@property (nonatomic, strong) NSString* takeoffTime;
@property (nonatomic, strong) NSString* landingTime;
@property (nonatomic, strong) NSString* flightCarrier;
@property (nonatomic, strong) NSString* flightNumber;
@property (nonatomic, strong) NSString* flightEq;
@property (nonatomic, strong) NSString* durationString;
@property (nonatomic) float price;

@property (nonatomic, readonly) int durationMinutes;

@end
