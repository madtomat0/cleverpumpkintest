//
//  CPTTripData.m
//  CleverPumpkinTest
//
//  Created by MAD Tomato on 11.07.13.
//  Copyright (c) 2013 MAD Tomato. All rights reserved.
//

#import "CPTTripData.h"

@implementation CPTTripData

-(void)setDurationString:(NSString *)durationString
{
    _durationString = durationString;
    
    NSScanner* timeScanner=[NSScanner scannerWithString:_durationString];
    int hours,minutes;
    [timeScanner scanInt:&hours];
    [timeScanner scanString:@":" intoString:nil];
    [timeScanner scanInt:&minutes];
    _durationMinutes = hours * 60 + minutes;
}

@end
