//
//  CPTMainTableViewController.m
//  CleverPumpkinTest
//
//  Created by MAD Tomato on 10.07.13.
//  Copyright (c) 2013 MAD Tomato. All rights reserved.
//

#import "CPTMainTableViewController.h"
#import "CPTTableViewCell.h"
#import "CPTTripsDataController.h"
#import "CPTConfig.h"

@interface CPTMainTableViewController () <CPTTripsDataControllerDelegate>
@property (nonatomic, strong) CPTTripsDataController* tripsDataController;
@property (nonatomic, strong) UIRefreshControl* refreshControl;
@property (strong, nonatomic) IBOutlet UIBarButtonItem* refreshBtn;
@property (strong, nonatomic) UIActivityIndicatorView* activityIndicator;
@end

@implementation CPTMainTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Available flights";
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] > 6.0) {
        NSMutableArray* toolbarItems = [self.toolbarItems mutableCopy];
        [toolbarItems removeObject:self.refreshBtn];
        self.refreshBtn = nil;
        [self setToolbarItems:toolbarItems];
        
        self.refreshControl = [[UIRefreshControl alloc] init];
        [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    }
    else {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.activityIndicator setHidesWhenStopped:YES];
        [self.navigationController.view addSubview:self.activityIndicator];
        self.activityIndicator.center = CGPointMake(self.navigationController.view.bounds.size.width * 0.5, self.navigationController.view.bounds.size.height * 0.5);
        
    }
    
    self.tripsDataController = [[CPTTripsDataController alloc] init];
    self.tripsDataController.delegate = self;
    
    [self refresh];
}

#pragma mark - Actions

- (IBAction)refresh
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] > 6.0) {
        [self.refreshControl beginRefreshing];
    }
    else {
        [self.activityIndicator startAnimating];
    }
    [self.tripsDataController loadXMLTripsData];
}

- (IBAction)sortTrips:(UIBarButtonItem *)sender
{
    if (sender.tag == 1) {
        [self.tripsDataController sortTripsBy:SortTypeByPrice];
    }
    else if (sender.tag == 2) {
        [self.tripsDataController sortTripsBy:SortTypeByDuration];
    }
    else NSAssert(0, @"No such button tag");
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tripsDataController.tripsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"CPTCell";
    CPTTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    CPTTripData* tripData = [self.tripsDataController.tripsArray objectAtIndex:indexPath.row];
    
    if (tripData) {
        cell.tripName.text = [NSString stringWithFormat:@"%@ - %@", tripData.takeoffCity, tripData.landingCity];
        cell.takeoff.text = [NSString stringWithFormat:@"Takeoff: %@ %@", tripData.takeoffDate, tripData.takeoffTime];
        cell.landing.text = [NSString stringWithFormat:@"Landing: %@ %@", tripData.landingDate, tripData.landingTime];
        cell.flight.text = [NSString stringWithFormat:@"Flight: %@ %@ %@", tripData.flightCarrier, tripData.flightNumber, tripData.flightEq];
        cell.duration.text = [NSString stringWithFormat:@"Duration: %@", tripData.durationString];
        cell.price.text = [NSString stringWithFormat:@"Price: %.2f", tripData.price];
    }
    else NSLog(@"Failed to load trip dictionary!");
    
    
    return cell;
}

#pragma mark - CPTTripsDataLoaderDelegate

-(void)tripsDataControllerFinishedParsing:(id)sender
{
    [self.tableView reloadData];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] > 6.0) {
        [self.refreshControl endRefreshing];
    }
    else {
        [self.activityIndicator stopAnimating];
    }
}

- (void)viewDidUnload
{
    [self setRefreshBtn:nil];
    [self setActivityIndicator:nil];
    [super viewDidUnload];
}
@end
