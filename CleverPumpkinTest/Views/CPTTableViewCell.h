//
//  CPTTableViewCell.h
//  CleverPumpkinTest
//
//  Created by MAD Tomato on 10.07.13.
//  Copyright (c) 2013 MAD Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPTTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel* tripName;
@property (strong, nonatomic) IBOutlet UILabel* takeoff;
@property (strong, nonatomic) IBOutlet UILabel* landing;
@property (strong, nonatomic) IBOutlet UILabel* flight;
@property (strong, nonatomic) IBOutlet UILabel* duration;
@property (strong, nonatomic) IBOutlet UILabel* price;
@end
