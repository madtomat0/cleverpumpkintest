//
//  CPTTableViewCell.m
//  CleverPumpkinTest
//
//  Created by MAD Tomato on 10.07.13.
//  Copyright (c) 2013 MAD Tomato. All rights reserved.
//

#import "CPTTableViewCell.h"

@implementation CPTTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
