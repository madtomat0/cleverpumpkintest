//
//  CPTConfig.h
//  CleverPumpkinTest
//
//  Created by MAD Tomato on 10.07.13.
//  Copyright (c) 2013 MAD Tomato. All rights reserved.
//

#define TRIPS_URL @"http://cleverpumpkin.ru/test/flights0541.xml"

#define kCPTResult @"result"
#define kCPTTrip @"trip"
#define kCPTDuration @"duration"
#define kCPTTakeoff @"takeoff"
#define kCPTDate @"date"
#define kCPTTime @"time"
#define kCPTCity @"city"
#define kCPTLanding @"landing"
#define kCPTFlight @"flight"
#define kCPTCarrier @"carrier"
#define kCPTNumber @"number"
#define kCPTEq @"eq"
#define kCPTPrice @"price"
