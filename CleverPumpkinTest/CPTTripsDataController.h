//
//  CPTTripsDataLoader.h
//  CleverPumpkinTest
//
//  Created by MAD Tomato on 10.07.13.
//  Copyright (c) 2013 MAD Tomato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPTTripData.h"

typedef enum {
    SortTypeByPrice = 1,
    SortTypeByDuration
} CPTSortType;

@protocol CPTTripsDataControllerDelegate;

@interface CPTTripsDataController : NSObject

@property (nonatomic, strong, readonly) NSMutableArray* tripsArray; // of CPTTripData objects
@property (nonatomic, weak) id<CPTTripsDataControllerDelegate> delegate;

- (void)loadXMLTripsData;
- (void)sortTripsBy:(CPTSortType)sortType;

@end

@protocol CPTTripsDataControllerDelegate <NSObject>
-(void) tripsDataControllerFinishedParsing:(id)sender;
@end
