//
//  CPTTripsDataLoader.m
//  CleverPumpkinTest
//
//  Created by MAD Tomato on 10.07.13.
//  Copyright (c) 2013 MAD Tomato. All rights reserved.
//

#import "CPTTripsDataController.h"
#import "AFNetworking.h"
#import "CPTConfig.h"

@interface CPTTripsDataController()<NSXMLParserDelegate>
@property (nonatomic, strong) NSMutableArray* tripsArray;
@property (nonatomic, strong) CPTTripData* currentTrip;
@property (nonatomic, strong) NSString* priceString;
@end

@implementation CPTTripsDataController

#pragma mark - Accessors

-(CPTTripData *)currentTrip
{
    if (!_currentTrip) {
        _currentTrip = [[CPTTripData alloc] init];
    }
    return _currentTrip;
}

-(NSMutableArray *)tripsArray
{
    if (!_tripsArray) _tripsArray = [[NSMutableArray alloc] init];
    return _tripsArray;
}

#pragma mark - Main Methods

- (void)loadXMLTripsData
{
    NSURL* url = [NSURL URLWithString:TRIPS_URL];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    
    AFXMLRequestOperation* operation = [AFXMLRequestOperation
            XMLParserRequestOperationWithRequest:request
        success:^(NSURLRequest* request, NSHTTPURLResponse* response, NSXMLParser* XMLParser)
    {
        XMLParser.delegate = self;
        [XMLParser setShouldProcessNamespaces:YES];
        [XMLParser parse];
    }
        failure:^(NSURLRequest* request, NSHTTPURLResponse* response, NSError* error, NSXMLParser* XMLParser)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Data"
                                                     message:[NSString stringWithFormat:@"%@",error]
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        
        [alertView show];
    }];
    
    [operation start];
}

-(void)sortTripsBy:(CPTSortType)sortType
{
    if (sortType == SortTypeByPrice) {
        [self.tripsArray sortUsingComparator:^(CPTTripData* obj1, CPTTripData* obj2) {
            
            if (obj1.price > obj2.price) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            
            if (obj1.price < obj2.price) {
                return (NSComparisonResult)NSOrderedAscending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }];
    }
    else if (sortType == SortTypeByDuration) {
        [self.tripsArray sortUsingComparator:^(CPTTripData* obj1, CPTTripData* obj2) {
            
            if (obj1.durationMinutes > obj2.durationMinutes) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            
            if (obj1.durationMinutes < obj2.durationMinutes) {
                return (NSComparisonResult)NSOrderedAscending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }];
    }
    else NSAssert(0, @"No such sort type!");
}

#pragma NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:kCPTResult]) {
        [self.tripsArray removeAllObjects];
    }
    
    if ([elementName isEqualToString:kCPTTrip]) {
        self.currentTrip.durationString = [attributeDict valueForKey:kCPTDuration];
    }
    
    if ([elementName isEqualToString:kCPTPrice]) {
        return;
    }
    
    if ([elementName isEqualToString:kCPTTakeoff]) {
        self.currentTrip.takeoffDate = [attributeDict valueForKey:kCPTDate];
        self.currentTrip.takeoffTime = [attributeDict valueForKey:kCPTTime];
        self.currentTrip.takeoffCity = [attributeDict valueForKey:kCPTCity];
    }
    
    if ([elementName isEqualToString:kCPTLanding]) {
        self.currentTrip.landingDate = [attributeDict valueForKey:kCPTDate];
        self.currentTrip.landingTime = [attributeDict valueForKey:kCPTTime];
        self.currentTrip.landingCity = [attributeDict valueForKey:kCPTCity];
    }
    
    if ([elementName isEqualToString:kCPTFlight]) {
        self.currentTrip.flightCarrier = [attributeDict valueForKey:kCPTCarrier];
        self.currentTrip.flightNumber = [attributeDict valueForKey:kCPTNumber];
        self.currentTrip.flightEq = [attributeDict valueForKey:kCPTEq];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.priceString = string;
}

- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:kCPTPrice]) {
        self.currentTrip.price = [self.priceString floatValue];
    }

    if ([elementName isEqualToString:kCPTTrip]) {
        [self.tripsArray addObject:self.currentTrip];
        self.currentTrip = nil;
    }
}

-(void) parserDidEndDocument:(NSXMLParser *)parser
{
    [self.delegate tripsDataControllerFinishedParsing:self];
}

@end
